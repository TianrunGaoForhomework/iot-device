import json
import logging
import paho.mqtt.client as mqttClient
from labs.common.DataUtil import DataUtil
'''
this is the MqttSubAppTest class, which is for
subscribe to the test topic and wait for at 
least 65 secs
'''
# set logging level
logging.basicConfig(level=logging.INFO)
'''
this is method is to subscribe a given topic 
@param clientConnect: client
@param userdata: data
@param flags : flag
@param resultCode : code
'''
def on_connect(clientConnect, userdata, flags, resultCode):
    print("Client connected to server. Result: " + str(resultCode))
    # subscribe topic 
    clientConnect.subscribe('AssignmentForMQTT', 1)

'''
receive payload message and display to console
@param clientConnect : client
@param userdata : data
@param  msg : message
'''    
def on_message(clientConnect, userdata, msg):
    # get payload message
    payload = str(msg.payload)
    # print to console
    print("Received Publish on topic {0}. Payload: {1}".format(str(msg.topic), payload))
    # get logger
    logging.info('Received Publish on topic')
    if not payload:
        return
    # strip the string 
    payloadtrip = payload.lstrip("b")
    # strip the string
    payloadtriptrip = payloadtrip.lstrip("'").rstrip("'")
    dataUtil = DataUtil();
    du=dataUtil.jsonToSensorData(payloadtriptrip)
    print(du)
    # load JSON format
    sdDict = json.loads(payloadtriptrip) 
    print("Received new config.")
    # get sdDict of name part
    sensorName = sdDict["name"]
    # get sdDict of timeStamp part
    sensorTime = sdDict["timeStamp"]
    # get sdDict of curValue part
    sensorCur  = sdDict["curValue"]
    # get sdDict of avgValue part
    sensorAvg  = sdDict["avgValue"]
    # get sdDict of minValue part
    sensorMin  = sdDict["minValue"]
    # get sdDict of maxValue part
    sensorMax  = sdDict["maxValue"]
    # get sdDict of sampleCount part
    sensorSam  = sdDict["sampleCount"]
    # print to the console
    print('   Sensor name is: \'{}\''.format(sensorName), '\n   Sensor timeStamp is: \'{}\''.format(sensorTime),
        '\n   Sensor curValue is: \'{}\''.format(sensorCur), '\n   Sensor avgValue is: \'{}\''.format(sensorAvg),
        '\n   Sensor minValue is: \'{}\''.format(sensorMin), '\n   Sensor maxValue is: \'{}\''.format(sensorMax),
        '\n   Sensor sampleCount is: \'{}\''.format(sensorSam))
    
    print("Converting object format to JSON format:")
    print(payloadtriptrip)
    logging.info('Backing to JSON format is done')

'''
Connect to the MQTT client, then: 1) If this is the 
subscribe app, subscribe to the given topic 
'''
def run():
    # create mqtt instance 
    mcqq = mqttClient.Client()
    # mqtt.connect
    mcqq.on_connect = on_connect
    # mqtt.message
    mcqq.on_message = on_message
    # mqtt connect
    mcqq.connect("iot.eclipse.org", 1883, 60)
    # loop
    mcqq.loop_forever()

'''
run MqttSubAppTest
'''
if __name__ == '__main__':  
    run()
    
    