'''
Created on Apr 16, 2019

@author: gaotianrun
'''
import sys
sys.path.append('/home/pi/gtr/iot-device/apps')
from time import sleep
from sense_hat import SenseHat
import threading

'''
this is senseHatLedActivator class, which is for control 
Led display including set the value and set the Lad value 
flag
'''
class SenseHatLedActivator(threading.Thread):
    # set enableLed as False
    enableLed = False
    # set rateInSec to 1 
    rateInSec = 1
    # set rotateDeg to 270
    rotateDeg = 270
    # set sh as None
    sh = None
    # set displayMsg as None
    displayMsg = None
    
    '''
    this is init method, which is for initialization
    '''
    def __init__(self, rotateDeg = 270, rateInSec = 1):
        super(SenseHatLedActivator, self).__init__()
        # if rateInSec > 0
        if rateInSec > 0:
            # get rataInSec
            self.rateInSec = rateInSec
        # if rotateDeg >= 0
        if rotateDeg >= 0:
            # get rotateDeg
            self.rotateDeg = rotateDeg
        # create instance of SenseHat
        self.sh = SenseHat()
        # set rotateDeg to sh
        self.sh.set_rotation(self.rotateDeg)
    
    '''
    this is run method
    '''
    def run(self):
        while True:
            # if enableLed is ture 
            if self.enableLed:
                # if displayMag is not none
                if self.displayMsg != None:
                    # set pixel list
                    self.sh.set_pixel_list = ()
                    # display the message    
                    self.sh.show_message(str(self.displayMsg))
            else:
                # display the message
                self.sh.show_letter(str('N'))
                # sleep at regular secs
                sleep(self.rateInSec)
                # clear the display
                self.sh.clear()
            # sleep at regular secs
            sleep(self.rateInSec)
            
    '''
    this is getRateInSeconds method for gettting 
    sleep secs
    '''
    def getRateInSeconds(self):
        return self.rateInSec
    
    '''
    this is setEnableLedFlag method for setting
    flag vlaue
    '''        
    def setEnableLedFlag(self, enable):
        # clear the display
        self.sh.clear()
        # set enableLed to enable
        self.enableLed = enable
        
    '''
    this is setDisplayMessage method for setting
    display message
    '''
    def setDisplayMessage(self, msg):
        # get mssage
        self.displayMsg = msg + "|got message|"