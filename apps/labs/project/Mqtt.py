'''
Created on Apr 17, 2019

@author: gaotianrun
'''

'''
this is the MqttSubAppTest class, which is for
subscribe to the test topic and wait for at 
least 65 secs
'''
import sys
sys.path.append('/home/pi/gtr/iot-device/apps')
import logging
import paho.mqtt.client as mqttClient
from labs.project import SenseHatLedActivator

# set logging level
logging.basicConfig(level=logging.INFO)
# create instance of SenseHatLedActivator
SensHat = SenseHatLedActivator.SenseHatLedActivator()

'''
this is method is to subscribe a given topic 
@param clientConnect: client
@param userdata: data
@param flags : flag
@param resultCode : code
'''
def on_connect(clientConnect, userdata, flags, resultCode):
    print("Client connected to server. Result: " + str(resultCode))
    # subscribe topic 
    clientConnect.subscribe('ActuatorData')

'''
receive payload message and display to console
@param clientConnect : client
@param userdata : data
@param  msg : message
'''    
def on_message(clientConnect, userdata, msg):
    print("Received PUBLISH on topic {0}. Payload: {1}".format(str(msg.topic), str(msg.payload)))
    # get string payload
    strr = str(msg.payload)
    #split the spring: example:{"timestamp": 1544219224392, "context": {}, "value": 38.0, "id": "5c0aea585916363eb0eaaf68"}
    #this code is to get the "value" of the msg.payload, which is 38 in the example
    print ("Request from the cloud: Set temp to" + str( strr.split(',')[-2].split(':')[1]) )
    tempData = str( strr.split(',')[-2].split(':')[1])
    # get string message
    message ="Set temp to" + tempData
    # print 
    print(message)
    print("Sense Hat start showing!")
    # set EnableLedFlag as True
    SensHat.setEnableLedFlag(True)
    # set display message
    SensHat.setDisplayMessage(message)

'''
Connect to the MQTT client, then: 1) If this is the 
subscribe app, subscribe to the given topic 
'''
def run():
    # create mqtt instance 
    mcqq = mqttClient.Client()
    # mqtt.connect
    mcqq.on_connect = on_connect
    # set daemon as True
    SensHat.daemon = True
    # start run
    SensHat.start()
    # mqtt.message
    mcqq.on_message = on_message
    # mqtt connect
    mcqq.connect("iot.eclipse.org", 1883, 60)
    # loop
    mcqq.loop_forever()

'''
run MqttSubAppTest
'''
if __name__ == '__main__':  
    run()