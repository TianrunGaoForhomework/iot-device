'''
Created on Apr 16, 2019

@author: gaotianrun
'''

import sys
sys.path.append('/home/pi/gtr/iot-device/apps')
from time import sleep
import threading
from labs.common import SensorData
from labs.common import ActuatorData
from coapthon.client.helperclient import HelperClient
from sense_hat import SenseHat
from labs.project import SmtpClientConnector
from labs.project import TempActuatorEmulator
from labs.project import CoapClientConnector

# create instance of TempActuatorEmulator
TempAcEmu = TempActuatorEmulator.TempActuatorEmulator()
# create instance of SensorData
Sens = SensorData.SensorData()
# create instance of HumData
sensForHum = SensorData.SensorData()
# create instance of ActuatorData
ActD = ActuatorData.ActuatorData()
# create instance of CoapClientConnecto
CoAP = CoapClientConnector.CoapClientConnector()
# create instance of SmtpClientConnector
SmtpConnector = SmtpClientConnector.SmtpClientConnector()
# create instance of sense
sense = SenseHat()

'''
This class is main class, for running whole Python file
first we got temperature and humidity from sensor and we
send to gateway by CoAP protocol, by the way if the alert was 
activated, an email will send to terminal
'''
class TempSensorAdaptor (threading.Thread):
    
    # create sleep time
    DEFAULT_RATE_IN_SEC = 10
    # set enableEmulator value as False
    enableEmulator = False
    # set isPrevTempSet value as False
    isPrevTempSet  = False
    # set sense name as temperature
    Sens.setName('Temperature')
    # set lowVal as 0
    lowVal = 0
    # set highVal as 30
    highVal = 30
    # set alertDiff as 1
    alertDiff = 1
    # create instance of ActD
    Data = ActD
    # set command as 0
    Data.setCommand(0)
    # set value as 0
    Data.setValue(0)
    # set error code as 0
    Data.setErrorCode(0)
    # set state data as 0
    Data.setStateData(0)
    # set status code as 0
    Data.setStatusCode(0)   
    '''
   this init method, which is for initialization
   @param rateInSec: DEFAULT_RATE_IN_SEC
    '''
    def __init__(self, rateInSec = DEFAULT_RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        if rateInSec > 0:
            self.rateInSec = rateInSec

    '''
    run method
    '''
    def run(self):
        while True:
            # when enableEmulator is true
            if self.enableEmulator:
                # get temperature from sensor
                self.curTemp = sense.get_temperature()   
                # get humidity from sensor
                self.curHum = sense.get_humidity()
                # add sensor temperature value 
                Sens.addValue(self.curTemp)
                # add sensor humidity value
                sensForHum.addValue(self.curHum)
                # print details
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.curTemp))
                # if isPrevTempSet is False 
                if self.isPrevTempSet == False:
                    # set prevTemp as curTemp
                    self.prevTemp = self.curTemp
                    # set isPrevTempSet as True
                    self.isPrevTempSet = True
                else:
                    
                    ########-- COAP Part --########
                    print('Transport data by protocol:COAP | Sending..... ')  
                    self.host = "172.20.10.2"         # set host
                    self.port = 5683                  # set port
                    self.path = 'temp'                # set path
                    # combine host and port
                    self.client = HelperClient(server=(self.host, self.port))    # combine port and host   
                    # add T to String
                    self.tcurTemp = 'T' + str(self.curTemp)
                    # add H to String
                    self.tcurHum = 'H' + str(self.curHum)
                    # post request 
                    self.response = self.client.post(self.path, str(self.tcurTemp))
                    # post request
                    self.response = self.client.post(self.path, str(self.tcurHum)) 
                   
                    ########-- Actuator Part --########
                    #Check the curTemp is higher or lower than nominalTemp
                    self.dif = TempAcEmu.Check(self.curTemp)
                    # if dif > 0
                    if (self.dif > 0):
                        print("Current temperature exceeds nomalTemp by --> " + str(self.dif) )
                        #If higher then set Command = 1, others is no needed for this module
                        TempAcEmu.setMessage(self.Data, 1, 0, None, 1)
                        # set value
                        self.Data.setValue(self.dif)
                        #Send the instance of ActuatorData to TempActuatorEmulator by processMessage
                        TempAcEmu.processMessage(self.Data)
                    # if dif < 0
                    if (self.dif < 0):
                        print("\nCurrent temperature falls below nomalTemp by -->" + str(abs(self.dif)) )
                        #If lower then set Command = 0
                        TempAcEmu.setMessage(self.Data, 0, 0, None, 1)
                        # set value
                        self.Data.setValue(abs(self.dif))
                        #Send the instance of ActuatorData to TempActuatorEmulator by processMessage
                        TempAcEmu.processMessage(self.Data) 
                    # if dif = 0
                    if (self.dif == 0):
                        # print information
                        print("Current temperature equal to nomalTemp.")
                        # print information 
                        print('Nothing need to do.') 
                             
                    ########-- Adaptor Part --########
                    print (Sens.__str__())
                    print ('\nCurTemp - AvgValue = ' + str(abs(self.curTemp - Sens.getAvgValue())))
                    print ('Threshold          = ' + str(self.alertDiff))
                    #Check if higher than threshold
                    if (abs(self.curTemp - Sens.getAvgValue()) >= self.alertDiff):
                        # print alertdiff
                        print('\nCurrent temp exceeds average by > ' + str(self.alertDiff) + '. Triggeringalert...')
                        # print to string 
                        self.sensorData = Sens.__str__()
                        #Used SMTP to send message to remote device.
                        SmtpConnector.publishMessage('Exceptional sensor data [test]', self.sensorData)
                # sleep at regular seconds
                sleep(self.rateInSec)


