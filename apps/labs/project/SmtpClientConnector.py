'''
Created on Apr 16, 2019

@author: gaotianrun
'''

import sys
sys.path.append('/home/pi/gtr/iot-device/apps')
import smtplib
from labs.common import ConfigUtil
from labs.common import ConfigConst
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

'''
this is SmtpClientConnector class, which is for sending
email service
'''
class SmtpClientConnector:
    '''
    init and read config file from the path
    '''
    def __init__(self):
        # load the config file
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        # load the config fiel
        self.config.loadConfig()
        # system print 
        print('Configuration data...\n' + str(self.config))
        # system print
        print('============= Setting Done! =============')
 
    #HOST_KEY,PORT_KEY,FROM_ADDRESS_KEY,TO_ADDRESS_KEY,USER_AUTH_TOKEN_KEY
    '''
    this method is to process sending details including 
    host, port, fromaddr, toaddr, authtoken
    @param topic : String
    @param data : Integer
    '''
    def publishMessage(self, topic, data):
        # get the host address from config file
        host = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY)
        # get the port from config file
        port = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY)
        # get the fromAddr from the config file
        fromAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY)
        # get the toAddr from the config file
        toAddr = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY)
        # get the authToken from the config file
        authToken = self.config.getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.USER_AUTH_TOKEN_KEY)
        # create instance of MIMEMultipart
        msg = MIMEMultipart()
        # load 'from'
        msg['From'] = fromAddr
        # load 'to'
        msg['To'] = toAddr
        # load 'subject'
        msg['Subject'] = topic
        # convert integer to string
        msgBody = str(data)
        # attach
        msg.attach(MIMEText(msgBody))
        # convert to string
        msgText = msg.as_string()
    
   
        # send e-mail notification
        #host and port info from sender
        smtpServer = smtplib.SMTP_SSL(host, port)
        #check state
        smtpServer.ehlo()
        #address and key from sender
        smtpServer.login(fromAddr, authToken)
        #sender address and receive address and text
        smtpServer.sendmail(fromAddr, toAddr, msgText)
        #close connection
        smtpServer.close()