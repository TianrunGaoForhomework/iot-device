'''
Created on Apr 16, 2019

@author: gaotianrun
'''
'''
this is the test class for running TempSensorAdaptor class
importing path fist and running the thread
'''
import sys
sys.path.append('/home/pi/gtr/iot-device/apps')

from time import sleep
from labs.project import TempSensorAdaptor

# create instance of TempSensorAdaptor
tempSensAdaptor = TempSensorAdaptor.TempSensorAdaptor()
# set daemon as True
tempSensAdaptor.daemon  = True
# print 
print('- - - - - - - - - - - - - - - - - - - - - - - - ')
print("Starting system performance app daemon thread...")
# set enableEmulator as True
tempSensAdaptor.enableEmulator = True
# start to run
tempSensAdaptor.start()

while (True):
    # sleep 15 secs
    sleep(15)
    pass