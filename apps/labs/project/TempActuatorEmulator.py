'''
Created on Apr 16, 2019

@author: gaotianrun
'''

import sys
sys.path.append('/home/pi/gtr/iot-device/apps')

from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.project import SenseHatLedActivator
from labs.common import ActuatorData

# create instance of SenseHatLedActivator
sensehat = SenseHatLedActivator.SenseHatLedActivator()
# create instance of ActuatorData
actuatordata = ActuatorData.ActuatorData()
# create instance of ConfigUtil
config = ConfigUtil.ConfigUtil()

'''
This is TempActuatorEmulator class, which is for process data from sensor 
when temperature warm, cold down. when temperature cold, warm up. And set
actuator
'''
class TempActuatorEmulator:
    '''
    init method, load configUtil file, set the normal temperature from 
    config and start to run
    
    '''   
    def __init__(self):
        # load config file
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        # load config file
        self.config.loadConfig()        
        # read file and get nominalTemp
        self.nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP)
        # print 
        print("The nomalTemp is = " + str(self.nominalTemp))
        # set daemon as true 
        sensehat.daemon = True
        # start to run
        sensehat.start()
        
    
    '''
    this method is to process data receiving from sensor
    when temperature warm, cold down. 
    when temperature cold, warm up
    @param data : Integer
    '''
    def processMessage(self, data):
        # get data 
        self.data = data
        # update actuatordata
        actuatordata.updateData(self.data)
        #determine if set msg cold or warm
        if (actuatordata.getCommand() == 1):
            # print msg warm up
            msg = "warm up"
            # set stateData msg
            actuatordata.setStateData(msg)
            # set led value
            sensehat.setLedValue(1)
        # when actuatordata get command o=0    
        if (actuatordata.getCommand() == 0):
            # print msg cool down
            msg = "cool down"
            # set actuatordata msg
            actuatordata.setStateData(msg)
            # set led value
            sensehat.setLedValue(0)
            #Enable sense_hat run()
            sensehat.setEnableLedFlag(True)
        # Send the msg to sense_hat
        message = "actuatorMessage :" +self.actuatordata.getStateData()
        # set display message
        sensehat.setDisplayMessage(message)
        
    '''
    this method is to set message including command 
    error code, state data, status code.
    @param actuatordata : Integer
    @param command : Integer
    @param errorcode : Integer
    @param stateData : Integer
    @param stateData : Integer
    @param statusCode : Integer
    '''            
    def setMessage(self, actuatordata, command, errcode, stateData, statusCode):
        # set command value
        actuatordata.setCommand(command)
        # set error code value
        actuatordata.setErrorCode(errcode)
        # set state data value
        actuatordata.setStateData(stateData)
        # set status code value
        actuatordata.setStatusCode(statusCode)
    
    '''
    this method is to compare the normal temperature 
    return the result
    @param curTemp: Integer
    '''
    def check(self, curTemp):
        # get curTemp value
        self.curTemp = curTemp
        # compute the difference
        self.dif = self.curTemp - float(self.nominalTemp)
        # get the difference
        dif = self.dif
        return dif