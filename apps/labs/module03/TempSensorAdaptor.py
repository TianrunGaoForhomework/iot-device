import threading
from time import sleep
from labs.common.SensorData import SensorData
from labs.module03.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03.TempActuatorEmulator import TempActuatorEmulator
from labs.common.ActuatorData import ActuatorData

class TempSensorAdaptor(threading.Thread):
    
    #init parameters
    RATE_IN_SEC   = 10
    lowVal        = 0
    highVal       = 30
    isPrevTempSet = False
    alertDiff     = 2
    
    #create object
    sensorData   = SensorData()
    connector    = SmtpClientConnector()
    sensehat     = SenseHat()
    tempactuator = TempActuatorEmulator()
    actuatordata = ActuatorData()
    #init actuatordata
    actuatordata.setValue(0)
    actuatordata.setCommand(0)
    actuatordata.setStateData(0)
    actuatordata.setErrorCode(0)

    #init thread    
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
    #overwrite run function        
    def run(self):
        while True:
            if self.enableEmulator:
            
                self.curTemp = self.sensehat.get_temperature()
                
                #add to sensor data
                self.sensorData.addValue(self.curTemp)
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
            
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    self.difference = self.tempactuator.check(self.curTemp)
                    
                    #check current temperature is higher or lower 
                    #if difference > 0 current temperature is higher we should set command as 0
                    if (self.difference > 0):
                        print("the difference is  " + str(self.difference) + ", the temperature need to cool down")
                        self.tempactuator.setMessage(self.actuatordata, 0, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        #send instance of actuatordata
                        self.tempactuator.processMessage(self.actuatordata)
                    
                    #if difference < 0 current temperature is lower we should set command as 1
                    if (self.difference < 0):
                        print("the difference is  " + str(self.difference) + "the temperature need to warm up")
                        self.tempactuator.setMessage(self.actuatordata, 1, 0, None, 1)
                        self.actuatordata.setValue(abs(self.difference))
                        #send instance of actuatordata
                        self.tempactuator.processMessage(self.actuatordata)
                         
                    if (self.difference == 0):
                        print("Nothing to change")
                   
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data', self.sensorData)
        
            sleep(self.rateInSec)
  
