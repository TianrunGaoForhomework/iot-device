"""
the script will simply initialize ‘tempactuatorEmulator’
and start the thread, then wait indefinitely
"""

from time import sleep
from labs.module03 import TempSensorAdaptor

tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
tempSensorAdaptor.enableEmulator = True
tempSensorAdaptor.start()
while (True):
    sleep(5)
    pass