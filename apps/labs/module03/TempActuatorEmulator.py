from labs.common import ConfigUtil
from labs.common import ConfigConst
from labs.module03.SenseHatLedActivator import SenseHatLedActivator
from labs.common.ActuatorData import ActuatorData


class TempActuatorEmulator:
    
    sensehat     = SenseHatLedActivator()
    actuatordata = ActuatorData()
    
    def __init__(self):
        #load config file
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        self.config.loadConfig()        
        #read file and get nominalTemp
        self.nominalTemp = self.config.getProperty(ConfigConst.CONSTRAINED_DEVICE, ConfigConst.NOMINAL_TEMP)
        print("The nomalTemp is = " + str(self.nominalTemp))
        
    def processMessage(self, data):
        self.data = data
        self.actuatordata.updateData(self.data)
        #determine if set msg cold or warm
        if (self.actuatordata == 1):
            msg = "warm up"
            self.actuatordata.setStateData(msg)
            
        if (self.actuatordata == 0):
            msg = "cool down"
            self.actuatordata.setStateData(msg)
            #Enable sense_hat run()
            self.sensehat.setEnableLedFlag(True)
            #Send the msg to sense_hat
            message = "actuatorMessage :" +self.actuatordata.getStateData()
            self.sensehat.setDisplayMessage(message)
            
    def setMessage(self, actuatordata, command, errcode, stateData, statusCode):
        actuatordata.setCommand(command)
        actuatordata.setErrorCode(errcode)
        actuatordata.setStateData(stateData)
        actuatordata.setStatusCode(statusCode)
        
    def check(self, curTemp):
        self.curTemp = curTemp
        self.dif = self.curTemp - float(self.nominalTemp)
        dif = self.dif
        return dif
         
        
        
        
    
        
    