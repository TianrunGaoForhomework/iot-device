"""
the script will simply initialize ‘tempactuatorEmulator’
and start the thread, then wait indefinitely
"""
from time import sleep
from labs.module04 import I2CSenseHatAdaptor
# creat an instance of i2csensenhatadaptor
i2csensenhatadaptor = I2CSenseHatAdaptor.I2CSenseHatAdaptor()
# set i2csensenhatadaptor.enableEmulator with true
i2csensenhatadaptor.enableEmulator = True
# start thread
i2csensenhatadaptor.start()
while (True):
    # rest in regular seconds
    sleep(5)
    pass
