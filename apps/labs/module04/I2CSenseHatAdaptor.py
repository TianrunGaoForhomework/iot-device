'''
for any rasp pi use
import sys
sys.path.append('/home/pi/workspace/connected-device-python')
'''
from time import sleep
from labs.common import ConfigUtil
import smbus
import threading

i2cBus              = smbus.SMBus(1)                  # Use I2C bus No.1 on Raspberry Pi3 +
enableControl       = 0x2D                            # Enable command    
enableMeasure       = 0x08                            # Enable command
accelAddr           = 0x1C                            # address for IMU (accelerometer)
magAddr             = 0x6A                            # address for IMU (magnetometer)
pressAddr           = 0x5C                            # address for pressure sensor
humidAddr           = 0x5F                            # address for humidity sensor
begAddr             = 0x28                            # beginning address
totBytes            = 6                               # read 1 to 6 bytes
DEFAULT_RATE_IN_SEC = 5                               # run at regular interval

'''
this is I2CSenseHatAdaptor class, used for reading data 
from the I2C buffers specified by the address for each sensor 
at regular interval
'''
class I2CSenseHatAdaptor(threading.Thread):
    rateInSec = DEFAULT_RATE_IN_SEC                   #set interval 
    
    '''
    init constructor, reading configuration file 
    '''
    def __init__(self):
        super(I2CSenseHatAdaptor, self).__init__()
        #get configuration path
        self.config = ConfigUtil.ConfigUtil('../../../data/ConnectedDevicesConfig.props')
        # load config
        self.config.loadConfig()
        #print details
        print('Configuration data...\n' + str(self.config))
        self.initI2CBus()

    '''
    init I2CBUS constructor 
    '''       
    def initI2CBus(self):
        print("Initializing I2C bus and enabling I2C addresses...")
        # write data for Accelrometer sensor 
        i2cBus.write_block_data(accelAddr, enableControl, enableMeasure)
        # write data for Magnetometer sensor 
        i2cBus.write_block_data(magAddr, enableControl, enableMeasure)
        # write data for Pressure sensor 
        i2cBus.write_block_data(pressAddr, enableControl, enableMeasure)
        # write data for Humidity sensor 
        i2cBus.write_block_data(humidAddr, enableControl, enableMeasure)
        # get Accelrometer sensor details
        self.AccelrometerData = i2cBus.read_i2c_block_data(accelAddr, begAddr, totBytes)
        # get Magnetometer sensor details
        self.MagnetometerData = i2cBus.read_i2c_block_data(magAddr, begAddr, totBytes)
        # get Pressure sensor details
        self.PressureData = i2cBus.read_i2c_block_data(pressAddr, begAddr, totBytes)
        # get Humidity sensor details
        self.HumidityData = i2cBus.read_i2c_block_data(humidAddr, begAddr, totBytes)
        
        '''
        display Accelrometer sensor details function
        @param data: AccelrometerData
        '''
        def displayAccelrometerData(self, data):
            #get Accelrometer data
            self.AccelrometerData = data
            # print to the monitor 
            print("Accelerometer block data:   " + str(self.AccelrometerData))
            
        '''
        display Magnetometer sensor details function
        @param data: MagnetometerData
        '''
        def displayMagnetometerData(self, data):
            #get Magnetometer data
            self.MagnetometerData = data
            # print to the monitor 
            print("Magnetometer block data:   " + str(self.MagnetometerData))
        
        '''
        display Pressure sensor details function
        @param data: PressureData
        '''
        def displayPressureData(self, data):
            #get Pressure data
            self.PressureData = data
            # print to the monitor
            print("Pressure block data:   " + str(self.PressureData))
        
        '''
        display Humidity sensor details function
        @param data: HumidityData
        
        '''
        def displayHumidityData(self, data):
            #get Pressure data
            self.HumidityData = data
            # print to the monitor
            print("Humidity block data:   " + str(self.HumidityData))
        
        '''
        overwrite thread run function
        '''
        def run(self):
            while True:
                # when enableEmulator is true
                if self.enableEmulator:
                    # call displayAccelerometerData function
                    self.displayAccelerometerData()
                    # call displayMagnetometerData function 
                    self.displayMagnetometerData()
                    # call displayPressureData function
                    self.displayPressureData()
                    # call displayHumidityData function
                    self.displayHumidityData()               
                # sleep in regular seconds
                sleep(self.rateInSec)
        
    
    