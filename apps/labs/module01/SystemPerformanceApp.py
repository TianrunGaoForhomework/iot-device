from time import sleep
from labs.module01 import SystemPerformanceAdaptor


sysPerfAdaptor = SystemPerformanceAdaptor.SystemPerformanceAdaptor()

  
# sysPerfAdaptor.daemon = True


sysPerfAdaptor.enableAdaptor = True

print("Starting system performance app daemon thread...")


sysPerfAdaptor.start()

while (True):
    sleep(5)
    pass
    