from time import sleep 


import threading

import psutil

class SystemPerformanceAdaptor (threading.Thread):
#     enableAdaptor = False
    RATE_IN_SEC = 10
    

    
#     def __init__(self, rateInSec):
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(SystemPerformanceAdaptor, self).__init__()
        self.rateInSec = rateInSec
#         self.RATE_IN_SEC = RATE_IN_SEC
        
   
            
    def run(self):
        while True:
            if self.enableAdaptor:
                
                print('\n--------------------')
                print('New system performance readings:')
                print(' ' + str(psutil.cpu_stats()))
                print(' ' + str(psutil.virtual_memory()))

            sleep(self.rateInSec)
            