"""
the script will simply initialize ‘tempSensorAdaptor’
and start the thread, then wait indefinitely
"""

from time import sleep
from labs.module05 import TempSensorAdaptor

# create an instance of tempSensorAdaptor
tempSensorAdaptor = TempSensorAdaptor.TempSensorAdaptor()
# set enableEmulator with true 
tempSensorAdaptor.enableEmulator = True
print("Created DataUtil Instance")
print("Starting Data Formatter app test...")
print("Starting tempadaptor app daemon Thread...")
# thread starts
tempSensorAdaptor.start()
while (True):
    # rest in regular seconds
    sleep(1)
    pass