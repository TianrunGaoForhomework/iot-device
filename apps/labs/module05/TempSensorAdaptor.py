'''
import setting 
'''
import json
import threading
from time import sleep
from labs.common.SensorData import SensorData
from labs.module03.SmtpClientConnector import SmtpClientConnector
from sense_hat import SenseHat
from labs.module03.TempActuatorEmulator import TempActuatorEmulator
from labs.common.ActuatorData import ActuatorData
from labs.common.DataUtil import DataUtil
'''
this is TempSensorAdaptor class, used for reading data 
and formatting that data using JSON, writing that data 
to a file and then reading that data using a separate 
application, finally displaying their output to the console.
'''
class TempSensorAdaptor(threading.Thread):
    
    RATE_IN_SEC   = 4                     # set RATE_IN_SEC with 4
    lowVal        = 0                     # set lowVal with 0
    highVal       = 30                    # set highVal with 30
    isPrevTempSet = False                 # set isPrevTempsSet with False
    alertDiff     = 2                     # set alertDiff with 2
    
    sensorData   = SensorData()           # create an instance of SensorData
    connector    = SmtpClientConnector()  # create an instance of SmtpClientConnector
    sensehat     = SenseHat()             # create an instance of SenseHat
    tempactuator = TempActuatorEmulator() # create an instance of TempActuatorEmulator
    actuatordata = ActuatorData()         # create an instance of ActuatorData
    datautil     = DataUtil()             # Create an instance of DataUtil
    
    #init actuatordata
    actuatordata.setValue(0)              # set value with 0
    actuatordata.setCommand(0)            # set command with 0
    actuatordata.setStateData(0)          # set state with 0
    actuatordata.setErrorCode(0)          # set error code with 0

    '''
    init thread
    @param rateInSec: RATE_IN_SEC
    @return: rateInSe
    '''  
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorAdaptor, self).__init__()
        self.rateInSec = rateInSec
        
    '''
    overwrite run function
    '''       
    def run(self):
        while True:
            # if enableEmulator is true
            if self.enableEmulator:
                # get curTemp from senseHat
                self.curTemp = self.sensehat.get_temperature()
                # add to sensor data
                self.sensorData.addValue(self.curTemp)
                # print to the console
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
                # if isPrevTempSet is False
                if self.isPrevTempSet == False:
                    # set prevTemp with curTemp
                    self.prevTemp = self.curTemp
                    # set isPrevTempSet with true
                    self.isPrevTempSet = True
                else:
                    # compare the temperature to the curTemp
                    self.difference = self.tempactuator.check(self.curTemp)
                    #check current temperature is higher or lower 
                    #if difference > 0 current temperature is higher we should set command as 0
                    if (self.difference > 0):
                        print("the difference is  " + str(self.difference) + ", the temperature need to cool down")
                        # set the message
                        self.tempactuator.setMessage(self.actuatordata, 0, 0, None, 1)
                        # set the value
                        self.actuatordata.setValue(abs(self.difference))
                        #send instance of actuatordata
                        self.tempactuator.processMessage(self.actuatordata)
                    
                    #if difference < 0 current temperature is lower we should set command as 1
                    if (self.difference < 0):
                        print("the difference is  " + str(self.difference) + "the temperature need to warm up")
                        # set the message
                        self.tempactuator.setMessage(self.actuatordata, 1, 0, None, 1)
                        # set the value
                        self.actuatordata.setValue(abs(self.difference))
                        #send instance of actuatordata
                        self.tempactuator.processMessage(self.actuatordata)
                    #if difference = 0     
                    if (self.difference == 0):
                        print("Nothing to change")
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + 'Converting data...')
                        # open result.json file, if not have, create it
                        with open("/Users/gaotianrun/Desktop/result.json", "w") as f:
                            # get data format
                            print(self.sensorData._dic_())
                            # write to the file
                            json.dump(self.sensorData._dic_(), f) 
                            # publish the message to e-mail
                            self.connector.publishMessage('Exceptional sensor data', self.sensorData._dic_())       
            sleep(self.rateInSec)
  
