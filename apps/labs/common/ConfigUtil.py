import configparser
import os
'''
get config file from path
'''
DEFAULT_CONFIG_FILE = "../../../data/ConnectedDevicesConfig.props"

# read config file
'''
this is ConfigUtil class, which is for getting config 
file and do several operation 
'''
class ConfigUtil:
    configFile = DEFAULT_CONFIG_FILE          # set configFile
    configData = configparser.ConfigParser()  # create an instance of configData
    isLoaded = False                          # set isLoaded with False
    
    '''
    init constructor
    '''
    def __init__(self, configFile = None):
        if (configFile != None):              # when configFile is not none
            self.configFile = configFile      # set configFile
    
    '''
    load config function
    '''    
    def loadConfig(self):
        if (os.path.exists(self.configFile)):     # when path exists
            self.configData.read(self.configFile) # read configFile
            self.isLoaded = True                  # set is Loaded with True
    
    '''
    get Config function
    @return: configData
    '''        
    def getConfig(self, forceReload = False):
        if (self.isLoaded == False or forceReload): # when is Loaded 
            self.loadConfig()                       # get loadConfig
        return self.configData

    '''
    @return: configFile
    '''
    def getConfigFile(self):
        return self.configFile

    '''
    @return: Config
    '''
    def getProperty(self, section, key, forceReload = False):
        return self.getConfig(forceReload).get(section, key)

    '''
    @return: isLoaded 
    '''
    def isConfigDataLoaded(self):
        return self.isLoaded

