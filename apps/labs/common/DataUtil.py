import json
from labs.common import ActuatorData
from labs.common import SensorData

'''
this is DataUtil class, which is for converting 
between  instance and JSON format
'''
class DataUtil():

    '''
    the function converting actuator data
    to JSON format
    @param ActuatorData: actuator data
    @return: JSON format of actuator data
    '''
    def ActuatorDataTojson(self, ActuatorData):
        
        codetoad = json.dumps(ActuatorData)
        return codetoad

    '''
    the function converting sensor data
    to JSON format
    @param SensorData: sensor data
    @return: JSON format of sensor data
    '''
    def SensorDataTojson(self, SensorData):
        
        codetosd = json.dumps(SensorData)
        return codetosd
    
    
    def toJson(self, name,timeStamp,avgValue,minValue,maxValue,curValue,totValue,sampleCount):
        
        json_info = {}
        json_info['name'] = name
        json_info['timeStamp'] = timeStamp
        json_info['avgValue'] = avgValue
        json_info['minValue'] = minValue
        json_info['maxValue'] = maxValue
        json_info['curValue'] = curValue
        json_info['totValue'] = totValue
        json_info['sampleCount'] = sampleCount
        print("Dictionary:  ")
        print(json_info)
            
        jsonData = json.dumps(json_info)
        # write data into json file

        print("JSon:  ")
        print(jsonData)
        
        return jsonData

    '''
    the function converting JSON format
    to actuator data
    @param jsonData: JSON actuator data
    @return: actuator data
    '''
    def jsonToActuatorData(self, jsonData):
        
        adDict = json.loads(jsonData)        # load jsonData
        ad = ActuatorData.ActuatorData()     # create an instance
        ad.name = adDict['name']             # parse name
        ad.timeStamp = adDict['timeStamp']   # parse timeStamp
        ad.hasError = adDict['hasError']     # parse hasError
        ad.command = adDict['command']       # parse command
        ad.errCode = adDict['errCode']       # parse errCode
        ad.statusCode = adDict['statusCode'] # parse statusCode
        ad.stateData = adDict['stateData']   # parse stateData
        ad.curValue = adDict['curValue']     # parse curValue

        return ad

    '''
    the function converting JSON format
    to sensor data
    @param jsonData: JSON sensor data
    @return: sensor data
    '''
    def jsonToSensorData(self, jsonData):
        
        sdDict = json.loads(jsonData)           # load jsonData
        sd = SensorData.SensorData()            # create an instance
        sd.name = sdDict['name']                # parse name
        sd.timeStamp = sdDict['timeStamp']      # parse timeStamp
        sd.avgValue = sdDict['avgValue']        # parse avgVaule
        sd.minValue = sdDict['minValue']        # parse minValue
        sd.maxValue = sdDict['maxValue']        # parse maxValue
        sd.curValue = sdDict['curValue']        # parse curValue
        sd.totValue = sdDict['totValue']        # parse totValue
        sd.sampleCount = sdDict['sampleCount']  #parse sampleCount

        return sd