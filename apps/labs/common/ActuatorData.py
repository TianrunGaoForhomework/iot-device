import os
from datetime import datetime

COMMAND_OFF   = 0                      # set command_0ff with 0
COMMAND_ON    = 1                      # set command_on with 1
COMMAND_SET   = 2                      # set command_set with 2
COMMAND_RESET = 3                      # set command_reset with 3
STATUS_IDLE   = 0                      # set status_idle with 0
STATUS_ACTIVE = 1                      # set status_active with 1
ERROR_OK              = 0              # set error_ok with 0
ERROR_COMMAND_FAILED  = 1              # set error_command_failed with 1
ERROR_NON_RESPONSIBLE = -1             # set error_non_responsibile with -1 

'''
this is ActuatorData class, which is for 
get a set of value
'''
class ActuatorData():

    timeStamp  = None                  # set timeStamp with none
    name       = 'Not set'             # set name
    hasError   = False                 # set haserror with false
    command    = 0                     # set command with 0
    errorCode  = 0                     # set errorcode with 0
    statusCode = 0                     # set statuscode with 0
    stateData  = None                  # set statedata with nonr
    val        = 0.0                   # set val with 0.0
    
    '''
    init constructor
    '''
    def __init__(self):
        self.updateTimeStamp()
    
    '''
    @return: command
    '''  
    def getCommand(self):
        return self.command
    
    '''
    @return: name
    '''
    def getName(self):
        return self.name
    
    '''
    @return: statedata
    '''
    def getStateData(self):
        return self.stateData
    
    '''
    @return: statuscode
    '''
    def getStatusCode(self):
        return self.statusCode
    
    '''
    @return: errorcode
    '''
    def getErrorCode(self):
        return self.errorCode
    
    '''
    @return: val
    '''
    def getValue(self):
        return self.val;
    
    '''
    @return: haserror
    '''
    def hasError(self):
        return self.hasError    
    
    '''
    @return: command
    '''
    def setCommand(self, command):
        self.command = command
    
    '''
    @return: name
    '''    
    def setName(self, name):
        self.name = name
        
    '''
    set stateData value
    '''
    def setStateData(self, stateData):
        self.stateData = stateData
     
    '''
    set statusCode value
    '''   
    def setStatusCode(self, statusCode):
        self.statusCode = statusCode
        
    '''
    set errorcode value
    '''
    def setErrorCode(self, errCode):
        self.errCode = errCode
        if (self.errCode != 0):        # when errorcode is not 0
            self.hasError = True       # set haserror with true     
        else:
            self.hasError = False      # set haserror with false
            
    '''
    set value
    @param val: value
    '''
    def setValue(self, val):
        self.val = val
    
    '''
    update details
    @param data: data
    '''    
    def updateData(self, data):
        self.command = data.getCommand()        # get command value
        self.statusCode = data.getStatusCode()  # get statusCode value
        self.errCode = data.getErrorCode()      # get errCode value
        self.stateData = data.getStateData()    # get stateData value
        self.val = data.getValue()              # get val value
    
    '''
    update timeStamp
    '''
    def updateTimeStamp(self):
        self.timeStamp = str(datetime.now())    # set tiempStamp
        
    '''
    converts the object and its attributes to a string
    value which will be displayed on console or 
    in trigger email
    @return: customStr
    '''
    def __str__(self):
        customStr = \
            str(self.name + ':' + \
                os.linesep + '\tTime: ' + self.timeStamp + \
                os.linesep + '\tCommand: ' + str(self.command) + \
                os.linesep + '\tStatus Code: ' + str(self.statusCode) + \
                os.linesep + '\tError Code: ' + str(self.errCode) + \
                os.linesep + '\tState Data: ' + str(self.stateData) + \
                os.linesep + '\tValue: ' + str(self.val))
        return customStr
        
    
    
    