import os
from datetime import datetime
'''
this class is sense data class, which is for setting 
a series of values
'''
class SensorData():
    timeStamp   = None                 # init timeStamp parameters with None              
    name        = 'Not set'        # init name parameters with Temperature 
    curValue    = 0                   # init curValue parameters with 0
    avgValue    = 0                   # init avgValue parameters with 0
    minValue    = 30                  # init minValue parameters with 20
    maxValue    = 0                   # init maxValue parameters with 0
    totValue    = 0                    # init toValue  parameters with 0
    sampleCount = 0                    # init sampleCount parameters with 0
    
    '''
    init constructor
    '''
    def __init__(self):
        self.timeStamp = str(datetime.now())
    
    '''
    add value function and check 
    temperature state 
    @param newVal: new vaule getting from sensor
    '''
    def addValue(self, newVal):
        self.sampleCount += 1                      # set sample
        self.timeStamp = str(datetime.now())       # get current timeStamp
        self.curValue = newVal                     # set curValue value
        self.totValue += newVal                    # set totValue value
        if (self.curValue < self.minValue):        # when self.curValue < self.minValue
            self.minValue = self.curValue          # set minValue with curValue
        if (self.curValue > self.maxValue):        # when self.curValue > self.maxValue
            self.maxValue = self.curValue          # set maxValue with curValue
        if (self.totValue != 0 and self.sampleCount > 0):  # when self.totValue != 0 and self.sampleCount > 0
            self.avgValue = self.totValue / self.sampleCount  #calculate average value
            
    '''
    @return: average value
    '''
    def getAvgValue(self):
        return self.avgValue
    
    '''
    @return: maximum value
    '''
    def getMaxValue(self):
        return self.maxValue
    
    '''
    @return: minimum value
    '''
    def getMinValue(self):
        return self.minValue
    
    '''
    @return: curValue
    '''
    def getValue(self):
        return self.curValue
    
    '''
    @return: name
    '''
    def setName(self, name):
        self.name = name
        
    '''
    converts the object and its attributes to a string
    value which will be displayed on console or 
    in trigger email
    @return: String
    '''
    def __str__(self):
        customStr = \
            str(self.name + ':' + \
            os.linesep + '\tTime: ' + self.timeStamp + \
            os.linesep + '\tCurrent: ' + str(self.curValue) + \
            os.linesep + '\tAverage: ' + str(self.avgValue) + \
            os.linesep + '\tSamples: ' + str(self.sampleCount) + \
            os.linesep + '\tMin: ' + str(self.minValue) + \
            os.linesep + '\tMax: ' + str(self.maxValue))
            
        return customStr
    
    def _dic_(self):
        sensordict = {
                    'name': self.name,
                    'timeStamp': self.timeStamp, 
                    'curValue': str(self.curValue), 
                    'avgValue': str(self.avgValue), 
                    'sampleCount': str(self.sampleCount), 
                    'minValue': str(self.minValue), 
                    'maxValue': str(self.maxValue)
                    }
        
        return sensordict