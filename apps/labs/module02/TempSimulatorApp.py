"""
the script will simply initialize ‘tempSensorEmulator’
and start the thread, then wait indefinitely
"""

from time import sleep
from labs.module02 import TempSensorEmulator

tempSensorEmulator = TempSensorEmulator.TempSensorEmulator()
tempSensorEmulator.enableEmulator = True
tempSensorEmulator.start()
while (True):
    sleep(5)
    pass
    