import threading
from time import sleep
from random import uniform
from labs.common.SensorData import SensorData
from labs.module02.SmtpClientConnector import SmtpClientConnector


class TempSensorEmulator(threading.Thread):
    #init parameters
    RATE_IN_SEC = 10
    lowVal = 0
    highVal = 30
    isPrevTempSet = False
    alertDiff = 5
    #create object
    sensorData = SensorData()
    connector = SmtpClientConnector()
    
    #init thread
    def __init__(self, rateInSec = RATE_IN_SEC):
        super(TempSensorEmulator, self).__init__()
        self.rateInSec = rateInSec
    #overwrite run function    
    def run(self):
        while True:
            if self.enableEmulator:
                #random generate number range from 0-30
                self.curTemp = uniform(float(self.lowVal), float(self.highVal))
                #add to sensor data
                self.sensorData.addValue(self.curTemp)
                print('\n--------------------')
                print('New sensor readings:')
                print(' ' + str(self.sensorData))
            
                if self.isPrevTempSet == False:
                    self.prevTemp = self.curTemp
                    self.isPrevTempSet = True
                else:
                    #If the threshold is reached or surpassed, data should be emailed to a remote service
                    if (abs(self.curTemp - self.sensorData.getAvgValue()) >= self.alertDiff):
                        print('\n Current temp exceeds average by > ' + str(self.alertDiff) + '. Triggering alert...')
                    self.connector.publishMessage('Exceptional sensor data [test]', self.sensorData)
        
            sleep(self.rateInSec)




